In this project, SEO Team requested to generate possible keywords searches.

I received data from site map, with H1-1 (headers) of our pages. 

Based on headers, I used regular expressions to extract as much information as possible. 

I then categorised the information in line with Quandoo's internal database and applied human-like logic
in the process of keywords creation. 

Code is being reused on several datasets and countries, since it dynamically follows logic used in pages' headers. 
